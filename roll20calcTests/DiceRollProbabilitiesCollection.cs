﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace roll20calcTests
{
    /// <summary>
    /// Class to hold a collection of roll probability sets
    /// </summary>
    /// <remarks>A single XML file could store roll results for 2d6, 3d6, 4d6, and so on...
    /// This class will be able to load such a file</remarks>
    [XmlRoot("AllRolls")]
    public class DiceRollProbabilitiesCollection
    {
		[XmlElement(IsNullable =true)]
        public string notes;

		[XmlArray("Results")]
		[XmlArrayItem("RollSet", typeof(DiceRollProbabilities))]
		public List<DiceRollProbabilities> RollSets;

		// Constructor
		public DiceRollProbabilitiesCollection()
		{
			RollSets = new List<DiceRollProbabilities>();
		}

		public void AddDiceRollProbability(DiceRollProbabilities probabilities)
		{
			RollSets.Add(probabilities);
		}
	}
}
