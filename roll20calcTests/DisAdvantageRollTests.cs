﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using roll20calc;

namespace roll20calcTests
{
    [TestFixture]
    public class DisAdvantageRollTests
    {
        Dictionary<int, double> _1d6ProbsDisadvantageDistribution;
        Dictionary<int, double> _1d8ProbsDisadvantageDistribution;
        Dictionary<int, double> _1d12ProbsDisadvantageDistribution;
        Dictionary<int, double> _1d20ProbsDisadvantageDistribution;

        #region Setup Code
        [SetUp]
        public void LoadProbabilityBaselines()
        {
            /*
                Normal Advantage   Disadvantage
            1	16.667%	2.778%	30.556%
            2	16.667%	8.333%	25.000%
            3	16.667%	13.889%	19.444%
            4	16.667%	19.444%	13.889%
            5	16.667%	25.000%	8.333%
            6	16.667%	30.556%	2.778%
            */
            _1d6ProbsDisadvantageDistribution = new Dictionary<int, double>
                {
                      { 1, 0.30556 }
                    , { 2, 0.25000 }
                    , { 3, 0.19444 }
                    , { 4, 0.13889 }
                    , { 5, 0.08333 }
                    , { 6, 0.02778 }
                };

            /*
                Normal	Advantage	Disadvantage
            1	12.50%	 1.563%	    23.438%
            2	12.50%	 4.688%	    20.312%
            3	12.50%	 7.813%	    17.188%
            4	12.50%	10.938%	    14.062%
            5	12.50%	14.063%	    10.938%
            6	12.50%	17.188%	     7.812%
            7	12.50%	20.313%	     4.688%
            8	12.50%	23.438%	     1.562%
             */
            _1d8ProbsDisadvantageDistribution = new Dictionary<int, double>
                {
                      {1, 0.23438 }
                    , {2, 0.20312 }
                    , {3, 0.17188 }
                    , {4, 0.14062 }
                    , {5, 0.10938 }
                    , {6, 0.07812 }
                    , {7, 0.04688 }
                    , {8, 0.01562 }
                };

            /*
                Normal	Advantage	Disadvantage
            1	8.33%	0.694%	15.972%
            2	8.33%	2.083%	14.583%
            3	8.33%	3.472%	13.194%
            4	8.33%	4.861%	11.806%
            5	8.33%	6.250%	10.417%
            6	8.33%	7.639%	9.028%
            7	8.33%	9.028%	7.639%
            8	8.33%	10.417%	6.250%
            9	8.33%	11.806%	4.861%
            10	8.33%	13.194%	3.472%
            11	8.33%	14.583%	2.083%
            12	8.33%	15.972%	0.694%
             */
            _1d12ProbsDisadvantageDistribution = new Dictionary<int, double>
                {
                     {1 ,0.15972 }
                    ,{2 ,0.14583 }
                    ,{3 ,0.13194 }
                    ,{4 ,0.11806 }
                    ,{5 ,0.10417 }
                    ,{6 ,0.09028 }
                    ,{7 ,0.07639 }
                    ,{8 ,0.06250 }
                    ,{9 ,0.04861 }
                    ,{10 ,0.03472 }
                    ,{11 ,0.02083 }
                    ,{12 ,0.00694 }
                };

            // I got the hard-coded numbers below from web searches on d20 roll probabilities
            // it turns out it's a linear relationship for each d20 value and 
            // the probability of it occurring
            _1d20ProbsDisadvantageDistribution = new Dictionary<int, double>();
            double probability = 0.0975;
            for (int ii = 1; ii <= 20; ii++)
            {
                _1d20ProbsDisadvantageDistribution.Add(ii, Math.Round(probability, 4));
                probability -= 0.005;
            }
        }
        #endregion

        [TestCase(6, 1)]
        [TestCase(8, 1)]
        [TestCase(12, 1)]
        [TestCase(20, 1)]
        public void DisadvantageRollProbabilityDistributions_MatchHandCalcs_1dX(int diesize, int numdice)
        {
            Dictionary<int, double> expected = null;

            switch (diesize)
            {
                case 6:
                    expected = _1d6ProbsDisadvantageDistribution;
                    break;
                case 8:
                    expected = _1d8ProbsDisadvantageDistribution;
                    break;
                case 12:
                    expected = _1d12ProbsDisadvantageDistribution;
                    break;
                case 20:
                    expected = _1d20ProbsDisadvantageDistribution;
                    break;
            }

            var r = new RollProbabilities(diesize, numdice, RollTypes.Disadvantage);

            Dictionary<int, double> actual = new Dictionary<int, double>();
            foreach (KeyValuePair<int, double> kv in r.Probabilities)
            {
                actual.Add(kv.Key, Math.Round(kv.Value, 5));
            }

            // normal probability distribution for a single die should be invariant 
            // with the modifier
            CollectionAssert.AreEqual(expected, actual);
        }
    }
}
