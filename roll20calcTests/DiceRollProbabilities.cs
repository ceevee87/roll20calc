﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace roll20calcTests
{
	/// <summary>
	/// Collection that stores probabilities for every possible roll result with n dice with d sides on each die
	/// </summary>
	/// <remarks>This is the meat of it. This is the class that store the possible roll results
	/// and the probability of rolling such a result. The roll results are for a single group
	/// of dice such as 4d6 or 2d8.</remarks>
    [XmlRoot("Results")]
	public class DiceRollProbabilities
	{
		[XmlElement]
		public int diesize;

		[XmlElement]
		public int numdice;

		[XmlArray("probabilities")]
		[XmlArrayItem("probability")]
		public List<IndexedDouble> probs;

		public DiceRollProbabilities() { }

		/// <summary>
		/// Initialize the properties of the class.
		/// </summary>
		/// <param name="d"></param>
		/// <param name="n"></param>
		/// <remarks>We don't call this guy from the constructor because Serialization requires a
		/// constructor without arguments</remarks>
		public void Initialize(int d, int n)
		{
			numdice = n;
			diesize = d;
			probs = new List<IndexedDouble>();
		}

		/// <summary>
		/// Write out just this probability table to an Xml file.
		/// </summary>
		/// <param name="FilePath"></param>
		/// <remarks>Not used</remarks>
        public void ToXml(string FilePath)
        {
			if (!File.Exists(FilePath)) return;

            XmlSerializer serializer = new XmlSerializer(typeof(DiceRollProbabilities));
            TextWriter writer = new StreamWriter(FilePath);

            serializer.Serialize(writer, this);
            writer.Close();
        }

		public Dictionary<int, double> GetProbabilities()
        {
			Dictionary<int, double> res = new Dictionary<int, double>();

			foreach (IndexedDouble id in probs)
			{
				if (!res.ContainsKey(id.Index))
                {
					res.Add(id.Index, Math.Round(id.Value, 6));
                }
			}

			return res;
        }
    }
}
