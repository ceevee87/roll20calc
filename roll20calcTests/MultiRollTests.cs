﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using NUnit.Framework;
using roll20calc;

namespace roll20calcTests
{
    [TestFixture]
    public class MultiRollTests
    {
        string _sTestDataRootDir = string.Format(@"{0}\{1}\"
                      , Path.GetDirectoryName(Directory.GetParent(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)).FullName)
                      , @"reference");


        private  DiceRollProbabilitiesCollection LoadRollProbabilityCollection(string FileName)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(DiceRollProbabilitiesCollection));
            TextReader reader = new StreamReader(FileName);

            DiceRollProbabilitiesCollection result = serializer.Deserialize(reader) as DiceRollProbabilitiesCollection;
            reader.Close();
            return result;
        }

        [TestCase(4, 2)]
        [TestCase(4, 3)]
        [TestCase(4, 4)]
        [TestCase(4, 5)]
        [TestCase(4, 6)]
        [TestCase(6, 2)]
        [TestCase(6, 3)]
        [TestCase(6, 4)]
        [TestCase(6, 5)]
        [TestCase(6, 6)]
        [TestCase(8, 2)]
        [TestCase(8, 3)]
        [TestCase(8, 4)]
        [TestCase(8, 5)]
        [TestCase(8, 6)]
        [TestCase(10, 2)]
        [TestCase(10, 3)]
        [TestCase(10, 4)]
        [TestCase(10, 5)]
        [TestCase(10, 6)]
        [TestCase(12, 2)]
        [TestCase(12, 3)]
        [TestCase(12, 4)]
        [TestCase(12, 5)]
        [TestCase(12, 6)]
        public void MultiRollProbabilitiesMatchBaseline(int diesize, int numdice)
        {
            string sReferenceDataFile = string.Concat(_sTestDataRootDir, string.Concat("mulit_rolls_d", diesize, ".xml"));
            Assert.IsTrue(File.Exists(sReferenceDataFile));

            DiceRollProbabilitiesCollection AllProbabilities = LoadRollProbabilityCollection(sReferenceDataFile);

            DiceRollProbabilities rollProbabilitiesRef = AllProbabilities.RollSets.FirstOrDefault(rs => rs.diesize == diesize && rs.numdice == numdice);

            Assert.NotNull(rollProbabilitiesRef);

            Dictionary<int, double> expected = rollProbabilitiesRef.GetProbabilities();

            var rollProbabilitiesCalculated = new RollProbabilities(diesize, numdice, RollTypes.Normal);

            Dictionary<int, double> actual = new Dictionary<int, double>();
            foreach (KeyValuePair<int, double> kv in rollProbabilitiesCalculated.Probabilities)
            {
                actual.Add(kv.Key, Math.Round(kv.Value, 6));
            }

            CollectionAssert.AreEqual(expected, actual);

        }
    }
}
