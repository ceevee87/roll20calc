﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using roll20calc;

namespace roll20calcTests
{
    [TestFixture]
    public class NormalRollTests
    {
        // all of these dictionaries have modifier values as their key
        // so, the basic tests will always have 0 as the key. this is just
        // like rolling a d20 without any modifier to your roll.

        Dictionary<int, double> _1d6ProbsNormalDistribution;
        Dictionary<int, double> _1d8ProbsNormalDistribution;
        Dictionary<int, double> _1d12ProbsNormalDistribution;
        Dictionary<int, double> _1d20ProbsNormalDistribution;

        //Dictionary<int, Dictionary<int, double>> d20ProbsMeetOrExceedTarget;

        #region Setup Code
        [SetUp]
        public void LoadProbabilityBaselines()
        {
            // cheat sheet probabilities. These were generated in an excel spreadsheet
            // which has all roll combinations
            /*
                Normal Advantage   Disadvantage
            1	16.667%	2.778%	30.556%
            2	16.667%	8.333%	25.000%
            3	16.667%	13.889%	19.444%
            4	16.667%	19.444%	13.889%
            5	16.667%	25.000%	8.333%
            6	16.667%	30.556%	2.778%
            */
            _1d6ProbsNormalDistribution = new Dictionary<int, double>();
            for (int ii = 1; ii <= 6; ii++)
            {
                _1d6ProbsNormalDistribution.Add(ii, Math.Round(.16666, 4));
            }

            /*
                Normal	Advantage	Disadvantage
            1	12.50%	1.563%	23.438%
            2	12.50%	4.688%	20.313%
            3	12.50%	7.813%	17.188%
            4	12.50%	10.938%	14.063%
            5	12.50%	14.063%	10.938%
            6	12.50%	17.188%	7.813%
            7	12.50%	20.313%	4.688%
            8	12.50%	23.438%	1.563%
             */
            _1d8ProbsNormalDistribution = new Dictionary<int, double>();
            for (int ii = 1; ii <= 8; ii++)
            {
                _1d8ProbsNormalDistribution.Add(ii, Math.Round(0.125, 3));
            }

            /*
                Normal	Advantage	Disadvantage
            1	8.33%	0.694%	15.972%
            2	8.33%	2.083%	14.583%
            3	8.33%	3.472%	13.194%
            4	8.33%	4.861%	11.806%
            5	8.33%	6.250%	10.417%
            6	8.33%	7.639%	9.028%
            7	8.33%	9.028%	7.639%
            8	8.33%	10.417%	6.250%
            9	8.33%	11.806%	4.861%
            10	8.33%	13.194%	3.472%
            11	8.33%	14.583%	2.083%
            12	8.33%	15.972%	0.694%
             */
            _1d12ProbsNormalDistribution = new Dictionary<int, double>();
            for (int ii = 1; ii <= 12; ii++)
            {
                _1d12ProbsNormalDistribution.Add(ii, Math.Round(0.083333, 4));
            }

            // probabilities for a d20 are pretty easy if we're not using advantage or
            // disadvantage. It's a 5% chance that you roll any single number
            _1d20ProbsNormalDistribution = new Dictionary<int, double>();
            for (int ii = 1; ii <= 20; ii++)
            {
                _1d20ProbsNormalDistribution.Add(ii, Math.Round(0.05, 2));
            }

            /*
            // probability to roll a 1 or higher on a d20 is 1.0 then it just goes down from
            // there. At 11 your chance is 50% and at 20 your chance of rolling a 20 is 5%
            d20ProbsMeetOrExceedTarget = new Dictionary<int, double>();
            for (int modifier=0; modifier <= 11; modifier++)
            {
                Dictionary<int, double> d = new Dictionary<int, double>();
                double probability = 1.0;
                for (int ii = modifier + 1; ii <= 20 + modifier; ii++)
                {
                    d.Add(ii, Math.Round(probability, 2));
                    probability -= 0.05;
                }
                d20ProbsMeetOrExceedTarget.Add(modifier, d);
            }
            */
        }
        #endregion

        // after re-work with how modifiers affect what is stored in the _probabilities collection
        // the baseline set up should only require a single dictionary from 1 to iDiesize. 
        // regardless of modifer that _probabilities collection should be invariant. 
        // also, the name of the test should be ProbabilitiesNormalMatchBaseline


        [TestCase(6, 1)]
        [TestCase(8, 1)]
        [TestCase(12, 1)]
        [TestCase(20, 1)]
        public void NormalRollProbabilityDistributions_MatchHandCalcs_1dX(int diesize, int numdice)
        {
            Dictionary<int, double> expected = null;

            switch (diesize)
            {
                case 6:
                    expected = _1d6ProbsNormalDistribution;
                    break;
                case 8:
                    expected = _1d8ProbsNormalDistribution;
                    break;
                case 12:
                    expected = _1d12ProbsNormalDistribution;
                    break;
                case 20:
                    expected = _1d20ProbsNormalDistribution;
                    break;
            }

            var r = new RollProbabilities(diesize, numdice, RollTypes.Normal);

            Dictionary<int, double> actual = new Dictionary<int, double>();
            foreach (KeyValuePair<int, double> kv in r.Probabilities)
            {
                actual.Add(kv.Key, Math.Round(kv.Value, 4));
            }

            // normal probability distribution for a single die should be invariant 
            // with the modifier
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestCase(6, 1)]
        [TestCase(8, 1)]
        [TestCase(12, 1)]
        [TestCase(20, 1)]
        public void NormalRollProbabilityDistributions_AllProbsSumTo1_1dX(int diesize, int numdice)
        {
            var r = new RollProbabilities(diesize, numdice, RollTypes.Normal);

            double totalProbability = Math.Round(r.Probabilities.Values.Sum(), 2);

            // normal probability distribution for a single die should be invariant 
            // with the modifier
            Assert.AreEqual(1.00, totalProbability);
        }

        [TestCase(6, 2)]
        [TestCase(8, 2)]
        [TestCase(12, 2)]
        [TestCase(20, 2)]
        [TestCase(6, 3)]
        [TestCase(8, 3)]
        [TestCase(12, 3)]
        [TestCase(6, 6)]
        [TestCase(8, 6)]
        [TestCase(12, 4)]
        public void NormalRollProbabilityDistributions_ProbablitiesAreSymmetric_NdX(int diesize, int numdice)
        {
            // example: imaging rolling 2d6. the probability of rolling a 4 is the same as rolling a 10. 
            var r = new RollProbabilities(diesize, numdice, RollTypes.Normal);

            int j = r.MaxRoll;
            int k = r.MinRoll;
            Assert.Multiple(() =>
            {
                for (; k < j; k++, j--)
                {

                    Assert.AreEqual(r[j], r[k]);
                }
            });
        }


        [Ignore("Test is busted")]
        [TestCase(6, 2, 8)]
        [TestCase(8, 3, 20)]
        [TestCase(12, 2, 10)]
        [TestCase(20, 1, 15)]
        public void RollIsLessThanTarget_Ndx(int diesize, int numdice, int target)
        {
            var r = new RollProbabilities(diesize, numdice, RollTypes.Normal);

            double expected = Math.Round(r.Probabilities.Where(t => t.Value < target).Sum(kv => kv.Value), 4);
            double actual = r.ProbabilityToRollLessThanTarget(target);
            Assert.AreEqual(expected, actual);
        }


        /*
        [TestCase(20, 0, 1)]
        [TestCase(20, 0, 5)]
        [TestCase(20, 0, 11)]
        [TestCase(20, 0, 15)]
        [TestCase(20, 0, 19)]
        public void RollsMeetOrExceedTarget_d20(int diesize, int modifier, int target)
        {
            var r = new RollProbabilities(diesize, modifier);
            r.CalcRollProbabilities(RollTypes.Normal);

            double actual = Math.Round(r.ProbabilityToRollOrExceed(target), 3);
            double expected = d20ProbsMeetOrExceedTarget[modifier][target];

            Assert.AreEqual(expected, actual);
        }
        */
    }
}
