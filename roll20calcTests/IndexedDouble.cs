﻿using System.Xml.Serialization;

namespace roll20calcTests
{
    /// <summary>
    /// Stores a single probability result for a single roll outcome.
    /// </summary>
    /// <remarks>A single instance of this class holds one roll outcome and its associated probability.
    /// Example: the chances of rolling a 7 (Index) is 16.666% (Value)
    /// 
    /// This class only exists as trickery to get the Xml serializer to serialize not just the probability
    /// values but the roll outcome too (Index property). 
    /// </remarks>
    public class IndexedDouble
    {
		[XmlAttribute("roll")]
		public int Index { get; set; }

		[XmlText]
		public double Value { get; set; }
	}
}
