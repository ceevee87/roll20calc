﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace roll20calc
{
    public class MonteCarlo
    {
        public readonly int DieSize;

        public readonly int NumDice;

        public readonly int NumSimulations;

        private readonly Random RandomGenerator;

        public Dictionary<int, double> Probabilities { get; }

        public int MinRoll => NumDice;

        public int MaxRoll => NumDice * DieSize;

        public double this[int index]
        {
            get => Probabilities[index];
        }

        public MonteCarlo(int iNumDice, int iDieSize, int iNumSimulations = 10000)
        {
            NumDice = iNumDice;
            DieSize = iDieSize;
            NumSimulations = iNumSimulations;
            RandomGenerator = new Random();
            Probabilities = new Dictionary<int, double>();

            Initialize();
        }

        private void Initialize()
        {
            if (NumDice < 0 || NumSimulations < 0)
            {
                throw new ArgumentOutOfRangeException("The number of dice and simulation runs must all be greater than 0.");
            }

            if (DieSize < 2)
            {
                throw new ArgumentOutOfRangeException("The number of die sides has to be at least 2.");
            }

            for (int ii = NumDice; ii <= NumDice * DieSize; ii++)
            {
                Probabilities.Add(ii, 0.0);
            }
        }

        public int Roll()
        {
            int rollTotal = 0;
            for (int ii = 0; ii < NumDice; ii++)
            {
                rollTotal += RandomGenerator.Next(1, DieSize + 1);
            }
            return rollTotal;
        }

        public void SimulateRolls()
        {
            Dictionary<int, int> rollResults = new Dictionary<int, int>();
            for (int ii = NumDice; ii <= NumDice * DieSize; ii++)
            {
                rollResults.Add(ii, 0);
            }

            for (int ii = 0; ii < NumSimulations; ii++)
            {
                int roll = Roll();
                if (!rollResults.ContainsKey(roll))
                {
                    // impossible
                    throw new ArgumentOutOfRangeException($"Simulated rool result of {roll} is outside range of probabilities collection.");
                }
                rollResults[roll] += 1;
            }

            // now calculate the probabilities
            foreach (int rollvalue in rollResults.Keys)
            {
                double probability = (double) rollResults[rollvalue] / (double) NumSimulations;
                Probabilities[rollvalue] = probability;
            }
        }
    }
}
