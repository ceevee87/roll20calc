﻿namespace roll20calc
{
    public enum ConsoleOutputTypes
    {
        Standard = 1,
        Cumulative = 2,
        Debug = 3
    }
}
