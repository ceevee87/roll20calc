﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace roll20calc
{
    class Program
    {
        /// <summary>
        /// Calculate roll probabilities for 1 or more dice of the same size.
        /// </summary>
        public static void Main(
            //char[] args, int argc
            int diesize = 20, int numdice = 1, int modifier = 0 

            //int? targetac, int diesize = 20, int modifier = 0, bool advantage = false, bool disadvantage = false  // these are the options for doing a dc/ac check
            //, bool dccontest=false, int m1=0, int m2=0, bool a1=false, bool a2=false, bool d1=false, bool d2=false // options for dc content
            //, bool dumptables=false
            )
        {
            //DealWithThisMess(targetac, diesize, modifier,advantage,disadvantage
            //, dccontest, m1, m2, a1, a2, d1, d2 
            //, dumptables);

            try
            {
                var rp = new RollProbabilities(diesize, numdice, modifier);
                rp.WriteToConsole(ConsoleOutputTypes.Debug);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("");
        }

        /// <param name="diesize">int value that specifies the number of sides of the die you're simulating.</param>
        /// <param name="targetac">int value of the dc or ac you're attempting to make.</param>
        /// <param name="modifier">int value of your ability modifier. Example:--modifier 2</param>
        /// <param name="advantage">if doing a ac/dc check specify if you're rolling with advantage</param>
        /// <param name="disadvantage">if doing a ac/dc check specify if you're rolling with disadvantage</param>
        private static void DealWithThisMess(
            int? targetac, int diesize = 20, int modifier = 0, bool advantage = false, bool disadvantage = false  // these are the options for doing a dc/ac check
            , bool dccontest = false, int m1 = 0, int m2 = 0, bool a1 = false, bool a2 = false, bool d1 = false, bool d2 = false // options for dc content
            , bool dumptables = false)
        {
            RollProbabilities attacker;
            RollProbabilities defender;

            int iDieSides = diesize;
            int iTarget;

            Console.WriteLine("");
            Console.WriteLine($"Probability calculations are for a d{iDieSides} die.");

            // we are just do an ac or dc check
            // that is, what is the probability of rolling a target dc/ac
            if (!dccontest)
            {
                if (disadvantage && advantage)
                {
                    Console.WriteLine("--disadvantage and --advantage cannot be simultaneously selected.");
                    Environment.Exit(-1);
                }

                if (!targetac.HasValue)
                {
                    Console.WriteLine("you must specify a target ac (it's the same as target dc). if doing a dc check use the --targetac option");
                    Environment.Exit(-2);
                }
                iTarget = targetac.Value;

                if (modifier < 0)
                {
                    Console.WriteLine("--modifier values need to be larger than 0.");
                    Environment.Exit(-4);
                }

                // should check if person specified --m1, --m2, --a1, --a2, --d1, --d2 values and if so
                // warn user that all these values will be ignored

                attacker = new RollProbabilities(iDieSides, modifier);

                if (advantage)
                {
                    attacker.CalcRollProbabilities(RollTypes.Advantage);
                }
                else if (disadvantage)
                {
                    attacker.CalcRollProbabilities(RollTypes.Disadvantage);
                }
                else
                {
                    attacker.CalcRollProbabilities(RollTypes.Normal);
                }
                Console.WriteLine($"Percent chance to hit AC {iTarget} is {String.Format("{0,6:0.00}", 100.00 * attacker.ProbabilityToRollOrExceed(iTarget))}%");
                //attacker.WriteToConsole();
            }
            else
            {
                // we are doing a dc contest!!
                // that's an attack v a defender both rolling dice and determing the probability
                // of the attacker winning
                // we need to have --m1, --m2 options specified
                // these options are optional:  --a1, --a2, --d1, --d2

                attacker = new RollProbabilities(iDieSides, m1);
                defender = new RollProbabilities(iDieSides, m2);

                if (a1)
                {
                    attacker.CalcRollProbabilities(RollTypes.Advantage);
                }
                else if (d1)
                {
                    attacker.CalcRollProbabilities(RollTypes.Disadvantage);
                }
                else
                {
                    attacker.CalcRollProbabilities(RollTypes.Normal);
                }

                if (a2)
                {
                    defender.CalcRollProbabilities(RollTypes.Advantage);
                }
                else if (d2)
                {
                    defender.CalcRollProbabilities(RollTypes.Disadvantage);
                }
                else
                {
                    defender.CalcRollProbabilities(RollTypes.Normal);
                }

                if (targetac.HasValue)
                {
                    // should warn user that this value will be ignored
                }

                Console.WriteLine($"Percent chance Attacker wins DC contest against Defender is {String.Format("{0,6:0.00}", 100.00 * attacker.dcContest(defender))}%");
                Console.WriteLine($"Percent chance Defender wins DC contest against Attacker is {String.Format("{0,6:0.00}", 100.00 * defender.dcContest(attacker))}%");
            }

            Console.WriteLine("");
        }
    }
}
