﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace roll20calc
{
    public static class Combinations
    {
        // anything more than 20! is just ridiculous for this program
        private static readonly long[] Factorials = new long[20]
            {
                 1
                ,1
                ,2
                ,6
                ,24
                ,120
                ,720
                ,5040
                ,40320
                ,362880
                ,3628800
                ,39916800
                ,479001600
                ,6227020800
                ,87178291200
                ,1307674368000
                ,20922789888000
                ,355687428096000
                ,6402373705728000
                ,121645100408832000
            };

        public static long Fact(int n)
        {

            if (n < 0)
            {
                throw new ArgumentOutOfRangeException($"Value of ${n} is less than zero. Factorials must be for positive numbers only.");
            }

            if (n >= 20)
            {
                throw new ArgumentOutOfRangeException($"Value of ${n} is greater than or equal to 20. Arugment must be in range of [0,19]");
            }

            return Factorials[n];
        }

        public static int NumberOfCombinations(int n, int k)
        {
            if (n < 0 || k < 0)
            {
                throw new ArgumentOutOfRangeException($"Value of ${k} or {n} is less than zero. Values must be positive integers.");
            }

            if (k > n)
            {
                throw new ArgumentOutOfRangeException($"Value of ${k} is greater {n}. value k must be less than n.");
            }

            if (k > 19)
            {
                throw new ArgumentOutOfRangeException($"Value of ${k} is greater than or equal to 20. Arugment must be in range of [0,19]");
            }

            if (k == 0) return 1;
            if (k == 1) return n;

            long res = 1;
            for (long ii = n; ii > n - k; ii--)
            {
                res *= ii;
            }
            res /= Factorials[k];

            if (res > int.MaxValue)
            {
                throw new ArithmeticException($"The number of n choose k combinations was greater {int.MaxValue}. What the hell?");
            }
            return (int) res;

        }

        internal static int UpperIndex(int s, int n, int k)
        {
            return (int) Math.Floor( (decimal) (k - n) / (decimal) s);
        }

        public static double F(int s, int n, int k)
        {

            if (k < n)
            {
                throw new ArgumentOutOfRangeException($"The value of {k} is less than the minimum value of {s}.");
            }

            if (k > n*s)
            {
                throw new ArgumentOutOfRangeException($"The value of {k} is more than the maximumn value of {s*n}.");
            }

            int sum = 0;
            for (int ii = 0; ii <= UpperIndex(s, n, k); ii++)
            {
                int partial = NumberOfCombinations(n, ii) * NumberOfCombinations(k - s * ii - 1, n - 1);

                if ((ii % 2) == 0)
                {
                    sum += partial;
                }
                else
                {
                    sum -= partial;
                }
            }

            return (double)sum / Math.Pow((double)s, (double)n);

        }
    }

}
