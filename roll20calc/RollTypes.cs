﻿namespace roll20calc
{
    public enum RollTypes
    {
        Normal = 1,
        Advantage = 2,
        Disadvantage = 3
    }
}
