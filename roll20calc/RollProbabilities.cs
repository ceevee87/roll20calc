﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace roll20calc
{
    public class RollProbabilities
    {
        public int DieSize { get; }

        private readonly int Modifier;

        public int NumDice;

        private readonly Dictionary<int, double> _probabilities;

        public Dictionary<int, double> Probabilities
        {
            get { return _probabilities; }
        }

        public int MinRoll => NumDice;

        public int MaxRoll => NumDice*DieSize;

        public double this[int index]
        {
            get => _probabilities[index];
        }

        public RollProbabilities(int iSize, RollTypes enRollType=RollTypes.Normal)
        {
            _probabilities = new Dictionary<int, double>();
            Modifier = 0;
            DieSize = iSize;
            NumDice = 1;
            Intialize(enRollType);
        }

        public RollProbabilities(int iSize, int iNumDice, RollTypes enRollType = RollTypes.Normal)
        {
            _probabilities = new Dictionary<int, double>();
            Modifier = 0;
            DieSize = iSize;
            NumDice = iNumDice;
            Intialize(enRollType);
        }

        public RollProbabilities(int iSize, int iNumDice, int iModifier, RollTypes enRollType = RollTypes.Normal)
        {
            _probabilities = new Dictionary<int, double>();
            Modifier = iModifier;
            NumDice = iNumDice;
            DieSize = iSize;
            Intialize(enRollType);
        }

        /// <summary>
        /// Create entries for all possible rolls. Set roll percentage to 0.0.
        /// </summary>
        private void Intialize(RollTypes enRollType)
        {
            // you can only do advantage or disadvantage with single die rolls

            if (NumDice > 1 && enRollType != RollTypes.Normal)
            {
                throw new ApplicationException("Cannot apply disadvantage or advantage style probabilities to multi-dice rolls.");
            }

            if (enRollType == RollTypes.Normal)
            {
                int j = MaxRoll;
                int k = MinRoll;
                for (; k <= j; k++, j--)
                {
                    double pct = Combinations.F(DieSize, NumDice, k);

                    _probabilities.Add(k, pct);
                    if (k != j) _probabilities.Add(j, pct);
                }
            }
            else
            {
                CalcRollProbabilities(enRollType);
            }
        }

        public void WriteToConsole(ConsoleOutputTypes outputType=ConsoleOutputTypes.Standard)
        {

            string headerline = $"Roll probabilities for {NumDice}d{DieSize}";
            if (Modifier > 0)
            {
                headerline = string.Concat(headerline, $" with a modifier of +{Modifier}");
            }
            Console.WriteLine(headerline);
            Console.WriteLine("");

            // need to do this stuff because of the order in which we inserted data
            // into the collection. We insert at the front and end then work toward the
            // the middle where middle is the average die roll value.
            var list = _probabilities.Keys.ToList();
            list.Sort();

            MonteCarlo mc = new MonteCarlo(NumDice, DieSize, 10000);

            if (outputType == ConsoleOutputTypes.Standard)
            {
                Console.WriteLine($"Roll  Probability");
                Console.WriteLine($"=================");
            }
            else if (outputType == ConsoleOutputTypes.Cumulative)
            {
                Console.WriteLine($"Roll  Probability  Cumulative");
                Console.WriteLine($"=============================");
            }
            else if (outputType == ConsoleOutputTypes.Debug)
            {
                Console.WriteLine($"Roll  Probability  Monte Carlo   Cumulative");
                Console.WriteLine($"===========================================");

                mc.SimulateRolls();
            }


            // cumulative means chance to roll this number or higher.
            // so the chance to roll the minimum roll or higher is always 1.00.
            double cumulativeProbability = 1.0;
            foreach (int key in list)
            {
                int roll = key + Modifier;
                string col1 = roll.ToString().PadLeft(2, ' ');
                string col2 = String.Format("{0,7:0.000}", 100.00 * _probabilities[key]);
                string col3 = String.Format("{0,8:0.0000}", 100.00 * Math.Round(cumulativeProbability, 6));

                if (outputType == ConsoleOutputTypes.Standard)
                {
                    Console.WriteLine($"{col1}    {col2}%");
                }
                else if (outputType == ConsoleOutputTypes.Cumulative)
                {
                    Console.WriteLine($"{col1}    {col2}%      {col3}%");
                }
                else if (outputType == ConsoleOutputTypes.Debug)
                {
                    string col4 = String.Format("{0,8:0.0000}", 100.00 * Math.Round(mc.Probabilities[key], 6));
                    Console.WriteLine($"{col1}    {col2}%    {col4}%      {col3}%");
                }
                cumulativeProbability -= _probabilities[key];
            }


        }

        /// <summary>
        /// Generate list of probabilities for each possible roll outcome.
        /// </summary>
        /// <param name="enRollType"></param>
        /// <returns></returns>
        public void CalcRollProbabilities(RollTypes enRollType)
        {
            // Note, doesn't affect the collection at all.  
            // you will always get a collection with values NumDice ... NumDice*DieSize
            // Modifier values are handled at the time of print out or calculations

            // iterate dice iDieSides * iDieSides "rolls" and record the outcome for each roll
            // if the RollType is Normal still roll iDieSides * iDieSides times for ease of coding
            Dictionary<int, int> possibleOutcomes = new Dictionary<int, int>();
            for (int ii = MinRoll; ii <= MaxRoll; ii++) { possibleOutcomes.Add(ii, 0); }

            int iTotalRolls = 0;

            for (int ii = MinRoll; ii <= MaxRoll; ii++)
            {
                for (int jj = MinRoll; jj <= MaxRoll; jj++)
                {
                    iTotalRolls++;
                    int rollResult = ii;

                    if (enRollType == RollTypes.Normal)
                    {
                        rollResult = ii;
                    }
                    else if (enRollType == RollTypes.Advantage)
                    {
                        rollResult = (ii >= jj) ? ii : jj;
                    }
                    else if (enRollType == RollTypes.Disadvantage)
                    {
                        rollResult = (ii <= jj) ? ii : jj;
                    }
                    possibleOutcomes[rollResult]++;
                }
            }

            // Now, do the percentages
            foreach (KeyValuePair<int, int> kvPair in possibleOutcomes)
            {
                _probabilities[kvPair.Key] = (double)kvPair.Value / (double)iTotalRolls;
            }
        }

        /// <summary>
        /// Sum of all probabilities that make the specified DC
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public double ProbabilityToRollOrExceed(int target)
        {
            int Target = target - Modifier;

            // really should throw an ArgumentOutOfRangeException
            if (Target < 1) return 1.00;

            // really should throw a ArgumentNullException
            if (_probabilities == null) return 1.00;

            if (Target > MaxRoll) return 0.00;
            if (Target <= MinRoll) return 1.0;

            List<int> rollsThatBeatDC = _probabilities.Keys.Where(t => t >= Target).ToList();

            double result = 0.0;
            foreach (int roll in rollsThatBeatDC)
            {
                result += _probabilities[roll];
            }

            // this is a big problem. Not sure how to flag this.
            if (result > 1.0)
            {
                result = 1.0;
            }
            return result;
        }

        /// <summary>
        /// Sum of all probabilities for rolls less than the specified target roll value
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public double ProbabilityToRollLessThanTarget(int target)
        {
            int Target = target - Modifier;

            // really should throw an ArgumentOutOfRangeException
            if (Target < 1) return 0.00;

            // really should throw a ArgumentNullException
            if (_probabilities == null) return 0.00;

            if (Target > MaxRoll) return 1.0;
            if (Target <= MinRoll) return 0.0;

            List<int> rollsThatMissTarget = _probabilities.Keys.Where(t => t < Target).ToList();

            double result = 0.0;
            foreach (int roll in rollsThatMissTarget)
            {
                result += _probabilities[roll];
            }

            // this is a big problem. Not sure how to flag this.
            if (result > 1.0)
            {
                result = 1.0;
            }
            return result;
        }

        /// <summary>
        /// probability to roll a specific target value
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public double ProbabilityToRoll(int target)
        {
            int Target = target - Modifier;

            if (Target < 1 || Target > DieSize) return 0.0;
            if (!_probabilities.ContainsKey(Target)) return 0.0;
            return _probabilities[Target];
        }

        /// <summary>
        /// Probability that an attacker (this) beats a defender in a DC contest
        /// </summary>
        /// <param name="defender"></param>
        /// <returns></returns>
        public double dcContest(RollProbabilities defender)
        {
            if (defender.DieSize != DieSize) throw new ArgumentException("Size of roll probabilities collections is not the same.");

            // there is no way the attacker can win a DC contest
            if (defender.MinRoll + defender.Modifier > MaxRoll + Modifier) return 0.0;

            // it's a slam dunk attacker wins DC contest
            if (MinRoll + Modifier > defender.MaxRoll + defender.Modifier) return 1.0;

            double result = 0.0;
            if (Modifier <= defender.Modifier)
            {
                for (int rr = MinRoll + defender.Modifier + 1; rr <= MaxRoll + Modifier; rr++)
                {
                    int attackerTarget = rr;
                    int defenderTarget = rr;
                    // Console.WriteLine($"Checking probability of attacker rolling a {attackerTarget} versus defender rolling less than {defenderTarget}.");
                    result += _probabilities[attackerTarget - Modifier] * defender.ProbabilityToRollLessThanTarget(defenderTarget);
                }
            }
            else
            {
                result = ProbabilityToRollOrExceed(MaxRoll + (Modifier - defender.Modifier) + 1);
                for (int rr = MinRoll + Modifier ; rr <= MaxRoll + (Modifier - defender.Modifier); rr++)
                {
                    int attackerTarget = rr;
                    int defenderTarget = rr;
                    //Console.WriteLine($"Checking probability of attacker rolling a {attackerTarget} versus defender rolling less than {defenderTarget}.");
                    result += _probabilities[attackerTarget - Modifier] * defender.ProbabilityToRollLessThanTarget(defenderTarget);
                }
            }
            return result;
        }
    }
}
